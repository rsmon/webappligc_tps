
package bal.produit;

import entites.Produit;

/**
 *
 * @author rsmon
 */
public interface BalProduit {
    
    Float caAnnuelProduit(Produit pProduit, int pAnnee);
    Float caMensuelProduit(Produit pProduit, int pAnnee, int pMois);   

    Float caAnneeEnCoursProduit(Produit pProduit);
    Float caMoisEnCoursProduit(Produit pProduit);
}
