package bal.region;

import bal.client.BalClient;
import entites.Client;
import entites.Region;
import java.io.Serializable;
import javax.inject.Inject;
import static utilitaires.UtilDate.*;

public class BalRegionImpl  implements BalRegion, Serializable{
    
    @Inject BalClient   balclient;
    
    @Override
    public Float caAnnuel(Region region, int pAnnee) {
        
        Float ca=0f;
        
        for (Client cli : region.getLesClients()) {
        
            ca+=balclient.caAnnuel(cli, pAnnee);
        }
        return ca;
    }

    @Override
    public Float caAnneeEnCours(Region region) {
        return caAnnuel(region, anneeCourante());
    }  
}
