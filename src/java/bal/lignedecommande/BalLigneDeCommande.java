
package bal.lignedecommande;

import entites.LigneDeCommande;

public interface BalLigneDeCommande {

    Float montantHtLigne(LigneDeCommande ldc);

    Float montantTTCLigne(LigneDeCommande ldc);
    
}
