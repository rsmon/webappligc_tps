
package webservice.rest.client;

import dto.client.ResumeClient;
import entites.Client;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import webservice.modele.ModeleWebServClient;

/**
 *
 * @author rsmon
 */
@Stateless
@Path("client")
public class WebServRestClient  {
    
    @Inject ModeleWebServClient modele;
    
    @GET
    @Path("caanneeencours/{numcli}")
    @Produces("text/plain")
    public String caClientAnneeenCours(@PathParam("numcli")Long numcli) {
 
       return modele.caAnneeEnCours(modele.getLeClient(numcli)).toString();
    }

    @GET
    @Path("numero/{numcli}")
    @Produces({"application/xml","application/json"})
     public Client getLeClient(@PathParam("numcli")Long numcli) {
    
       Client c= modele.getLeClient(numcli);
       modele.creerArbreClientV2(c);
       return c;
    }
    
    @GET
    @Path("tous")
    @Produces({"application/xml","application/json"})
    public List<Client> getLesClients(){
    
     List<Client> lesClients=modele.getTousLesClients();
     
     for(Client c : lesClients ){
         
        modele.creerArbreClientV1(c);
     }
     
     return lesClients;
    }
 
    @GET
    @Path("tous/resumes")
    @Produces({"application/xml","application/json"})
    public List<ResumeClient> getTousLesResumeClients() {
        
        return modele.getTousLesResumeClients();
    }
    
}
