
package webservice.soap.pourtest.client;

import dao.Dao;
import dao.region.DaoRegion;
import entites.Client;
import entites.Region;
import javax.inject.Inject;
import javax.jws.Oneway;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import webservice.modele.ModeleWebServClient;

/**
 *
 * @author rsmon
 */
@WebService(serviceName = "WebServiceTesteurClient")
public class WebServiceTesteurClient {

    
    @Inject Dao dao;
    @Inject ModeleWebServClient  modeleClient;
    @Inject DaoRegion            daoRegion;
    
    @WebMethod(operationName = "getClient")
    public Client getClient(@WebParam(name = "pNumcli") Long pNumcli) {
        
        Client cli=modeleClient.getLeClient(pNumcli);
        
        modeleClient.creerArbreClientV1(cli);
    
        return cli;
    }

    @WebMethod(operationName = "getCaAnnuelClient")
    public Float getCaAnnuelClient(@WebParam(name = "pNumcli") Long pNumcli) {
        
        return modeleClient.caAnneeEnCours(modeleClient.getLeClient(pNumcli));
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "modifierClient")
    @Oneway
    public void modifierClient(@WebParam(name = "pClient") Client pClient) {
    
      dao.repercuterMAJ(pClient);
    }

    @WebMethod(operationName = "ajouterClient")
    @Oneway
    public void ajouterClient(@WebParam(name = "pClient") Client pClient) {
    
      dao.enregistrerEntite(pClient);
    }
    
    @WebMethod(operationName = "getRegion")
   
    public Region getRegion(@WebParam(name = "pCoderegion") String pCoderegion) {
    
      Region region=this.daoRegion.getLaRegion(pCoderegion);
      dao.detache(region);
      region.setLesClients(null);
      return region;
    }

    //
    
    @WebMethod(operationName = "supprimerClient")
    @Oneway
    public void supprimerClient(@WebParam(name = "pClient") Client pClient) {
    
     dao.supprimerEntite(pClient);
    }
    
    
    
    
    
}
