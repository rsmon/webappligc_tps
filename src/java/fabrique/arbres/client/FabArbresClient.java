
package fabrique.arbres.client;

import entites.Client;

public interface FabArbresClient {
    
    
    /**
    * Client <br/>
    * - 1 Region <br/>
    */ 
    void creerArbreClientV1(Client client);
    
    
    
     /**
     * Client <br/>
     * - 1 Region <br/>
     * - x Commande <br/>
     * ---- x LigneDeCommande <br/>
     * ------- 1 Produit <br/>
     * ---------- 1 CategorieProduit<br/> 
     */
     void creerArbreClientV2(Client client);
   
}
