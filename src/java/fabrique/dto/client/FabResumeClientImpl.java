package fabrique.dto.client;

import bal.client.BalClient;
import dao.client.DaoClient;
import dto.client.ResumeClient;
import entites.Client;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class FabResumeClientImpl implements Serializable, FabResumeClient{

    @Inject DaoClient    daoclient;
    @Inject BalClient    balclient;
   
    @Override
    public ResumeClient getResumeClient(Client pClient){
    
      ResumeClient rc=new ResumeClient();
      
      rc.setNumCli(pClient.getNumCli());
      rc.setNomCli(pClient.getNomCli());
      rc.setAdrCli(pClient.getAdrCli());
      
      rc.setCodereg(pClient.getLaRegion().getCodeRegion());
      rc.setNomreg(pClient.getLaRegion().getNomRegion());
      
      rc.setCaAnneeEnCours(balclient.caAnneeEnCours(pClient));
      rc.setSolde(-balclient.resteARegler(pClient));
      
      return rc;
    } 
    
    @Override
    public ResumeClient getResumeClient(Long pNumcli){
     
       return  getResumeClient(daoclient.getLeClient(pNumcli));
    }
     
    @Override
    public List<ResumeClient> getTousLesResumeClients(){
    
      return getLesResumeClients(daoclient.getTousLesClients());
    
    }
       
    private List<ResumeClient> getLesResumeClients(List<Client>  pListeClients ){
   
        List<ResumeClient> lc = new LinkedList<ResumeClient>();
        
        for(Client c : pListeClients){
        
           lc.add(getResumeClient(c));
        }
        return lc;
    }
       
}
