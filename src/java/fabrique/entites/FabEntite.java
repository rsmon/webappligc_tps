
package fabrique.entites;

import entites.CategorieProduit;
import entites.Client;
import entites.Commande;
import entites.LigneDeCommande;
import entites.Produit;
import entites.Region;
import fabrique.entite.categorieproduit.FabEntiteCategorieProduit;
import fabrique.entite.client.FabEntiteClient;
import fabrique.entite.commande.FabEntiteCommande;
import fabrique.entite.lignedecommande.FabEntiteLigneDeCommande;
import fabrique.entite.produit.FabEntiteProduit;
import fabrique.entite.region.FabEntiteRegion;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 *
 * @author rsmon
 */

@Singleton
public class FabEntite {
   
    @Inject FabEntiteRegion           fabRegion;
    @Inject FabEntiteClient           fabClient;
    @Inject FabEntiteCategorieProduit fabCategorieProduit;
    @Inject FabEntiteProduit          fabProduit; 
    @Inject FabEntiteCommande         fabCommande;
    @Inject FabEntiteLigneDeCommande  fabLigneDeCommande;
    
    public Region creerEntiteRegion(String codeRegion, String nomRegion) {
        return fabRegion.creerEntiteRegion(codeRegion, nomRegion);
    }
    
    public Client creerEntiteClient(Long numCli, String nomCli, String adrCli, Region laRegion) {
        return fabClient.creerEntiteClient(numCli, nomCli, adrCli, laRegion);
    }

    public CategorieProduit creerCategorieProduit(String codeCateg, String nomCateg) {
        return fabCategorieProduit.creerCategorieProduit(codeCateg, nomCateg);
    }

    public Produit creerProduit(String refProd, String desigProd, Float prixProd, CategorieProduit categProd) {
        return fabProduit.creerProduit(refProd, desigProd, prixProd, categProd);
    }

    public Commande creerCommande(Long numCom, String dateCom, String etatCom, Client leClient) {
        return fabCommande.creerCommande(numCom, dateCom, etatCom, leClient);
    }

    public LigneDeCommande creerLigneDeCommande(Commande laCommande, Produit leProduit, Float qteCom) {
        return fabLigneDeCommande.creerLigneDeCommande(laCommande, leProduit, qteCom);
    }

      
}
