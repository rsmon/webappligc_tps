package fabrique.entite.commande;

import entites.Client;
import entites.Commande;
import static utilitaires.UtilDate.parse;

/**
 *
 * @author rsmon
 */
public class FabEntiteCommande {
    
    
    public Commande creerCommande(Long numCom,String dateCom, String etatCom, Client leClient) {
        
        Commande commande=new Commande();
        
        commande.setNumCom(numCom);
        commande.setDateCom(parse(dateCom));
        commande.setEtatCom(etatCom);
        commande.setLeClient(leClient);
        
        return commande;
    }
    
}
