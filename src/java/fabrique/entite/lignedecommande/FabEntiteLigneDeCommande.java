package fabrique.entite.lignedecommande;

import entites.Commande;
import entites.LigneDeCommande;
import entites.Produit;

/**
 *
 * @author rsmon
 */
public class FabEntiteLigneDeCommande {

    public LigneDeCommande creerLigneDeCommande(Commande laCommande, Produit leProduit, Float qteCom ) {
       
        LigneDeCommande ligneDeCommande= new LigneDeCommande(); 
        
        ligneDeCommande.setLaCommande(laCommande);
        ligneDeCommande.setLeProduit(leProduit);
        ligneDeCommande.setQteCom(qteCom);
       
      
        return ligneDeCommande;
    }
}
