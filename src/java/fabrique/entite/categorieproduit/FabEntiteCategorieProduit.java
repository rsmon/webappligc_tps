package fabrique.entite.categorieproduit;

import entites.CategorieProduit;
import javax.inject.Singleton;

/**
 *
 * @author rsmon
 */

@Singleton
public class FabEntiteCategorieProduit {
   
    public CategorieProduit creerCategorieProduit(String codeCateg, String nomCateg){
    
       CategorieProduit categorieproduit=new CategorieProduit();
       
       categorieproduit.setCodeCateg(codeCateg);
       categorieproduit.setNomCateg(nomCateg);
      
       return categorieproduit;
    
    }
    
}
